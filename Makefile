# @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
# simple makefile to manage this project


all: build


build:
	./build.sh


clean:

	./clean.sh


doc: README.wiki

	- mkdir doc
	- ls README.wiki | sed -ne 's/.wiki//p' | xargs -I {}  echo "wiki-tool/mediawiki2texi.py {}.wiki {}.info {} >{}.texinfo; makeinfo --force --html {}.texinfo; makeinfo {}.texinfo; cat {}.info" | sh
	- rm *info
	- ls README.wiki | sed -ne 's/.wiki//p' | xargs -I {}  echo "cp {}/index.html doc; rm -r {}" | sh



test: test/

	- cd test; ./test.py


.PHONY: build clean all doc test
