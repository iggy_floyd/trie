/****h* BK-TREE/trie
  *  NAME
  *    trie realization of the BK-tree
  *  
  *  COPYRIGHT
  *    "(c) 2014 by <Unister Gmbh/Igor Marfin>" 
  *  
  *  SYNOPSIS
  *    
  *
  *
  *  DESCRIPTION
  *    * definition of the node
  *    * definition of the tree
  *    * Estimate timing of the search  of entries in BK-Tree and Cayley processing
  *
  *  INPUTS
  *
  *  RESULT
  *
  *  EXAMPLE
  *
  *  NOTES
  *    
  *  BUGS
  *  SEE ALSO
  *    BK-TREE/httpserver
  *  |html <img src="figs/classtrees_1_1bktree__coll__graph.png"> 
  ******
  * You can use this space for remarks that should not be included
  * in the documentation.
  */



#ifndef _TRIE_OPT_HPP_
#define _TRIE_OPT_HPP_

/*
const int MY_LOGGER = 0; // Unique identifier for our logger
#define CLOG(id) __FILE__, __LINE__, id
namespace tools {
void clog_error(const char *sfile, int sline, int id, const char *fmt, ...);
 }
*/


#include <fstream>
#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <cmath>
        

#include <iostream>
#include <string>
#include <algorithm>
#include <cctype>
#include <sstream>
#include <algorithm>


#include <boost/foreach.hpp>
#include <boost/shared_ptr.hpp>


#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/shared_ptr.hpp>

#include <boost/serialization/serialization.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>



#include "rdestl-read-only/hash.h"
#include "rdestl-read-only/hash_map.h"
#include "rdestl-read-only/stack_allocator.h"

 
namespace trees {

	using namespace std;



	// Trie's node
	struct trie_opt
	{
            
	    //typedef map<char, trie_opt*> next_t;
            typedef rde::hash_map<char, trie_opt*, rde::hash<char>, 8,rde::equal_to<char> > next_t;
            //typedef rde::hash_map<char, trie_opt*, rde::hash<char>, 8> next_t;

            
 
	    // The set with all the letters which this node is prefix
	    next_t next;
 
	    // If word is equal to "",  there is no word in the
	    //  dictionary; this is a end
	    string word;

        int counter; // to keep how many different entries but with the same key was remembered, The counter is needed by remove

	    trie_opt() : next(next_t()),counter(0) {}
            
 
	    void insert(string w);

	    bool remove(const string & w);

	    void randomization() {}

	private:

        	friend class boost::serialization::access;
	        template<class Archive>
                void serialize(Archive & ar, const unsigned int version)
                {
                        ar &  next;
                        ar &  word;
                        ar & counter;
                }


	}; // trie



 namespace trie_detail {

         void transform(std::string & word);


// The minimum cost of a given word to be changed to a word of the dictionary
	template< typename KeyType, typename MetricType>
	void search_impl_min_cost(trees::trie_opt* tree, char ch, MetricType * last_row, const KeyType & word,KeyType & closest, MetricType & min_cost)
	{
		const int sz = word.size()+1;
		MetricType current_row[sz];

	 	current_row[0] = last_row[0] + 1;
 
	    	// Calculate the min cost of insertion, deletion, match or substitution
		MetricType insert_or_del, replace;
		for (int i = 1; i < sz; ++i) {
        		insert_or_del = min(current_row[i-1] + 1, last_row[i] + 1);
		        replace = (word[i-1] == ch) ? last_row[i-1] : (last_row[i-1] + 1);
 		        current_row[i] = min(insert_or_del, replace);
		}
 
		// When we find a cost that is less than the min_cost,  because
		// it is the minimum found up-to the current row,  we update
		if ((current_row[sz-1] < min_cost) && (tree->word != "")) {
	        	min_cost = current_row[sz-1];
	        	closest = tree->word;
		}
 
	   	// If there is an element which is smaller than the current minimum cost,
	   	//  we can have another cost smaller than the current minimum cost
	 	if (*min_element(current_row, current_row + sz) < min_cost) {
       			for (trie_opt::next_t::iterator it = tree->next.begin(); it != tree->next.end(); ++it) {
		        search_impl_min_cost<KeyType, MetricType>(it->second, it->first, current_row, word,closest,min_cost);
		        }
		}
	}


	template< typename KeyType, typename MetricType>
	void search_min_cost(trees::trie_opt* tree, KeyType word, KeyType & closest, MetricType & min_cost)
	{
		word = string("$") + word;
		const int sz = word.size();
		min_cost = 0x3f3f3f3f;
 
		MetricType current_row[sz + 1];
 
		// Naive DP initialization
		for (int i = 0; i < sz; ++i) current_row[i] = i;
		current_row[sz] = sz;

		// For each letter in the root map wich matches with a
		//  letter in word, we must call the search
		for (int i = 0 ; i < sz; ++i) {
	 	       if (tree->next.find(word[i]) != tree->next.end()) {
		            search_impl_min_cost<KeyType, MetricType>(tree->next[word[i]], word[i], current_row, word,closest,min_cost);
		        }
	 	}
 
	}


template< typename KeyType, typename MetricType>
	void search_impl_within_distance(trees::trie_opt* tree, char ch, MetricType * last_row, const KeyType & word, MetricType max_cost, map<KeyType,MetricType> & results)
	 {
 
		 int sz =  word.size() + 1;
		 MetricType  current_row[sz];

		current_row[0] = last_row[0] + 1;
		MetricType _ins, _del, _rep;
		for (int i = 1; i < sz; ++i) {
			_ins =current_row[i-1] + 1;
			_del= last_row[i] + 1;
			_rep = (word[i-1] == ch) ? last_row[i-1] : (last_row[i-1] + 1);
			current_row[i] = min(_ins, min(_del,_rep));
		}

		// it is the minimum until the current row, so we update
		if ((current_row[sz-1] <= max_cost) && (tree->word != "")) {
			results[tree->word] = current_row[sz-1];
		}

		if (*min_element(current_row, current_row+sz) <= max_cost) {
			for (trie_opt::next_t::iterator it = tree->next.begin(); it != tree->next.end(); ++it)
				search_impl_within_distance<KeyType, MetricType>(it->second, it->first, current_row, word,max_cost,results);
		}

 
	}

	template< typename KeyType, typename MetricType>
	void search_within_distance(trees::trie_opt* tree,  KeyType  word, MetricType max_cost, map<KeyType,MetricType> & results)
	{
		word = string("$") + word;
		const int sz = word.size();
		MetricType  current_row[sz + 1];

		// Naive DP initialization
		for (int i = 0; i < sz; ++i) current_row[i] = i;
		current_row[sz] = sz;


		// For each letter in the root map wich matches with a
		// letter in word, we must call the search
		for (int i = 0 ; i < sz; ++i) {
			if (tree->next.find(word[i]) != tree->next.end()) 
			search_impl_within_distance<KeyType, MetricType>(tree->next[word[i]], word[i], current_row, word,max_cost,results);
		}

	}



	template< typename KeyType, typename MetricType>
	void search_impl_within_distance(trees::trie_opt* tree, char ch, MetricType * last_row, const KeyType & word, MetricType max_cost, std::vector<std::pair<KeyType,MetricType>> & results)
	 {
 
		 int sz =  word.size() + 1;
		 MetricType  current_row[sz];

		current_row[0] = last_row[0] + 1;
		MetricType _ins, _del, _rep;
		for (int i = 1; i < sz; ++i) {
			_ins =current_row[i-1] + 1;
			_del= last_row[i] + 1;
			_rep = (word[i-1] == ch) ? last_row[i-1] : (last_row[i-1] + 1);
			current_row[i] = min(_ins, min(_del,_rep));
		}
		// it is the minimum until the current row, so we update
		if ((current_row[sz-1] <= max_cost) && (tree->word != "")) {
			results.push_back(std::make_pair(tree->word, current_row[sz-1]));
		}

		if (*min_element(current_row, current_row+sz) <= max_cost) {
        		for (trie_opt::next_t::iterator it = tree->next.begin(); it != tree->next.end(); ++it)
	        	search_impl_within_distance<KeyType, MetricType>(it->second, it->first, current_row, word,max_cost,results);
		}

 
	}



	template< typename KeyType, typename MetricType>
	void search_within_distance(trees::trie_opt* tree,  KeyType word,MetricType max_cost, std::vector<std::pair<KeyType,MetricType>> & results)
	{
		word = string("$") + word;
		const int sz = word.size();
		MetricType current_row[sz + 1];

		// Naive DP initialization
		for (int i = 0; i < sz; ++i) current_row[i] = i;
		current_row[sz] = sz;



		// For each letter in the root map wich matches with a
		// letter in word, we must call the search
		for (int i = 0 ; i < sz; ++i) {
			if (tree->next.find(word[i]) != tree->next.end()) 
			search_impl_within_distance<KeyType, MetricType>(tree->next[word[i]], word[i], current_row, word,max_cost,results);
		}

	}




	int similarity2distance( double similarity,string & word);
	double distance2similarity (int dist, string & word);

	}


} //trees::




#endif
