/* 
 * File:   trie_detail.h
 * Author: debian
 *
 * Created on December 18, 2014, 1:26 PM
 */

#ifndef TRIE_DETAIL_H
#define	TRIE_DETAIL_H

#include <string>
#include <cmath>

#include <iostream>
#include <string>
#include <algorithm>
#include <cctype>
#include <sstream>
#include <algorithm>

#include "tools/tools-text.hpp"
 
namespace trees {

	using namespace std;
        
        namespace trie_detail {

	void transform(string & word) 
	{

		if (word.empty()) return;
		const string _w = word;
                // a new format: use '|||' delimiter
            const std::string delim("|||");
            word = tools::boostsplit<string>(_w, delim)[0];

	}



	int similarity2distance( double similarity,string & word)
	{

		double len = word.size();
		if (len < 1e-6) return  int(1e6);
	//	return (int) ceil(similarity * len);
		return (int) floor(similarity * len);

	}

	double distance2similarity (int dist, string & word)
	{

	double len = word.size();
	if (len < 1e-6) return  1e0;
	return max(0.,1-dist/len);

	}



	}  // trees::trie_detail::

}

#endif	/* TRIE_DETAIL_H */

