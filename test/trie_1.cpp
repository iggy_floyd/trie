
#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <map>
#include  <cstring>
#include  <sstream>
#include <fstream>
#include  <locale>

#include <boost/thread/mutex.hpp>


#include "tools/cpu-time.hpp"
#include "tools/vprintf.hpp"



#include "trie.hpp"
#include "trie_opt.hpp"
#include "trie_sti.hpp"


#include <chrono>
#include <thread>

using namespace std;







 void gen_random(char *s, const int len) {
     for (int i = 0; i < len; ++i) {
         int randomChar = rand()%(26+26+10);
         if (randomChar < 26)
             s[i] = 'a' + randomChar;
         else if (randomChar < 26+26)
             s[i] = 'A' + randomChar - 26;
         else
             s[i] = '0' + randomChar - 26 - 26;
     }
     s[len] = 0;
 }


template <
        typename StorageType, 
        typename ResultType, 
        typename KeyType,
        typename MetricType 
>
struct tree_searcher
{

void operator() (StorageType *  store, ResultType * res, KeyType key,MetricType dist) {
    if (store != nullptr && res != nullptr){
        KeyType closest(""); 
        MetricType min_cost=0; 
        trees::trie_detail::transform(key); 
        trees::trie_detail::search_min_cost<KeyType,MetricType>(store,key,closest,min_cost); 
        if (trees::trie_detail::distance2similarity(min_cost,key)>=1-dist) (*res).push_back(std::pair<KeyType, MetricType>(closest,MetricType(trees::trie_detail::distance2similarity(min_cost,key))));
    }
}
};


const std::string  test()
{



std::ostringstream ss;
std::locale l("de_DE.UTF-8");

std::ifstream file("./BookerNames10K.txt");
//std::ifstream file("./BookerNames100K.txt");

// data to be processed: 3M entries
//std::ifstream file("./BookerNames1M.txt");
std::ifstream file2("./BookerNames1M.txt");
//std::ifstream file3("./BookerNames1M.txt");
//std::ifstream file2("./BookerNames10K.txt");
std::ifstream file3("./BookerNames10K.txt");
std::ifstream file4("./BookerNames10K.txt");
std::ifstream file5("./BookerNames10K.txt");
//std::ifstream file2("./BookerNames100K.txt");
//std::ifstream file3("./BookerNames100K.txt");


trees::trie trie;
typedef tree_searcher<trees::trie,std::vector<std::pair<std::string,double>>,std::string,double> SEARCHERTYPE;

//trees::trie_opt trie;
//typedef tree_searcher<trees::trie_opt,std::vector<std::pair<std::string,double>>,std::string,double> SEARCHERTYPE;

//trees::trie_sti trie;
//typedef tree_searcher<trees::trie_sti,std::vector<std::pair<std::string,double>>,std::string,double> SEARCHERTYPE;


std::vector<std::string> tosearch;
std::vector<std::string> tosearch_orig;


/*
 *  some parameters of this test
 *   
 *  rate_of_exactness --  its inverse defines how oft the property should be found the container
 * 
 *  delay     --   in the real life,  there is a delay (it might be 10 ms or 1s or 10s  or ...) between "search calls"
 * 
 */

const double rate_of_exactness = 0.005; // only one among 200 properties should be found in the container

// assume that  we have a delay of 200 ms on average. Depends on different factors: insert time in the tree, communication with Cayley, 
// other code in the communicator.cpp,  rules (Double Booking, Overall) calculation, php background etc
size_t delay = 0;

double wallread=tools::get_wall_time();        
if (file.is_open() && !file.eof() ) {
    
    std::string line;     
    getline(file, line);
    if (!line.empty()) {
        // random string of size 10
        int sz=5;   
        
        char aa[sz+1];        
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);
    

        trie.insert(std::string(bb)+line+"-"+std::string(aa));
        tosearch.push_back(std::string(bb)+line+"-"+std::string(aa));
        tosearch_orig.push_back(line);
        
    }
    while (!file.eof()) {
      getline(file, line);
    if (!line.empty()) {
     
        // random string of size 10
        int sz=5;   
        
        char aa[sz+1];
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);
    
    
        trie.insert(std::string(bb)+line+"-"+std::string(aa));
        tosearch.push_back(std::string(bb)+line+"-"+std::string(aa));
        tosearch_orig.push_back(line);

    }
      
    }
    
    file.close();
    std::cout<<"read has been finished\n";
}




if (file2.is_open() && !file2.eof() ) {
    
    std::string line;     
    getline(file2, line);
    if (!line.empty()) {
        // random string of size 10
        int sz=5;   
        char aa[sz+1];
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);
        
        trie.insert(std::string(bb)+line+"-"+std::string(aa));
        
    }
    while (!file2.eof()) {
      getline(file2, line);
    if (!line.empty()) {
     
        // random string of size 10
        int sz=5;   
        char aa[sz+1];
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);   

        trie.insert(std::string(bb)+line+"-"+std::string(aa));

    }
      
    }
    file2.close();
    std::cout<<"read has been finished\n";
}
/*
if (file3.is_open() && !file3.eof() ) {
    
    std::string line;     
    getline(file3, line);
    if (!line.empty()) {
        // random string of size 10
        int sz=5;   
        char aa[sz+1];
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);
    
        trie.insert(std::string(bb)+line+"-"+std::string(aa));

        
    }
    while (!file3.eof()) {
      getline(file3, line);
    if (!line.empty()) {
     
        // random string of size 10
        int sz=5;   
        char aa[sz+1];
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);
    
    
        trie.insert(std::string(bb)+line+"-"+std::string(aa));


    }
      
    }
    file3.close();
    std::cout<<"read has been finished\n";
}

if (file4.is_open() && !file4.eof() ) {
    
    std::string line;     
    getline(file4, line);
    if (!line.empty()) {
        // random string of size 10
        int sz=5;   
        char aa[sz+1];
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);
    
        trie.insert(std::string(bb)+line+"-"+std::string(aa));

        
    }
    while (!file4.eof()) {
      getline(file4, line);
    if (!line.empty()) {
     
        // random string of size 10
        int sz=5;   
        char aa[sz+1];
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);
    
    
        trie.insert(std::string(bb)+line+"-"+std::string(aa));


    }
      
    }
    file4.close();
    std::cout<<"read has been finished\n";
}
*/
/*
if (file5.is_open() && !file5.eof() ) {
    
    std::string line;     
    getline(file5, line);
    if (!line.empty()) {
        // random string of size 10
        int sz=5;   
        char aa[sz+1];
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);
    
        trie.insert(std::string(bb)+line+"-"+std::string(aa));

        
    }
    while (!file5.eof()) {
      getline(file5, line);
    if (!line.empty()) {
     
        // random string of size 10
        int sz=5;   
        char aa[sz+1];
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);
    
    
        trie.insert(std::string(bb)+line+"-"+std::string(aa));


    }
      
    }
    file5.close();
    std::cout<<"read has been finished\n";
}
*/




wallread=tools::get_wall_time()-wallread;   


double wall01=0.,dt_wall01= 0.,wall02=0.;
int count=0,count_search=0;

size_t exactness = 1/rate_of_exactness;
typedef std::vector<std::pair<std::string,double> > searcherResultType;
std::map<std::string,searcherResultType  > results; 

for (unsigned int i =tosearch.size()-1 ;i>=tosearch.size()-200;i--) {
    count_search++;
    std::cout<<i<<"\n";
    searcherResultType *_tmpres = new searcherResultType();
    SEARCHERTYPE searcher;
    
    std::string srch = (i%(exactness) == 0)?tosearch[i]:tosearch_orig[i];
    wall01=tools::get_wall_time();         
    searcher(&trie,_tmpres,srch,1.-0.80); // get with similarity of 0.80    
    dt_wall01 += tools::get_wall_time() - wall01;
    results[srch] = *_tmpres;
    delete _tmpres;
    
        
    
    
    // a possible delay
    std::this_thread::sleep_for(std::chrono::milliseconds(delay)); // in ms 
    
}

for (auto& p : results) {
    if (p.second.size()>0) {
        count++;
                std::cout<<p.first<<" found to be connected to "<<p.second[0].first <<" with "<< p.second[0].second <<"\n";
    }
    else        std::cout<<p.first<<" is not connected " <<"\n";
            }



std::cout<<"Read time : "<<wallread<<"\n";

std::cout<<"\nAssumed delay between calls... "<<delay<<" ms\n";
std::cout<<"\nTotal connected properties ... "<<count<<"\n";
std::cout<<"\nTotal time spent for "<< count_search <<" search operations... "<<dt_wall01<<" s\n";
std::cout<<"\nAverage time spent for search... "<< dt_wall01/count_search <<" s\n";
std::cout<<"\nLatency time ... "<< wall02-wall01 <<" s\n";


return ss.str();

}




#include <boost/python.hpp>

BOOST_PYTHON_MODULE(trie_1)
{
using namespace boost::python;
def("test", test);

}



