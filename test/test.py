#! /usr/bin/env python
# -*- coding: utf-8 -*-


'''
#  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
#  All rights reserved.
#
#
#  Usage: %(scriptName)s
#

'''


from subprocess import Popen,PIPE
import inspect
import sys


sys.path.append('.')



def test_cpp(file,incs="",args=""):

 cmd="python-config --cflags"
 cmd=cmd.split()
 (pyc,err) = Popen(cmd, stdout=PIPE).communicate()
 pyc=pyc.replace("-Wstrict-prototypes","");
 pyc=pyc.replace("\n","");
 cmd="g++  -std=c++0x  -I. %s   -I/usr/local/include 	   %s -fpic %s.cpp -shared -lboost_python %s -o %s.so"%(pyc,incs,file,args,file)
# print cmd
# return 
 cmd=cmd.split()
 (out,err) = Popen(cmd, stdout=PIPE).communicate()
 import importlib
 mymodule = importlib.import_module(file, package=file)
 
 return getattr(mymodule,"test")()

  




def hello():
   """  hello.cpp

   >>> hello()
   'hello, world'
   """
   return test_cpp(str(inspect.stack()[0][3]))
   


def trie_1():
   r"""  trie_1.cpp
         encode( '", \\, \\t, \\n' )
   >>> trie_1()
   igor-marfin-zeuthenersrt-15732|||1_ID|||traveller with 0.931034
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()

   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))






if __name__ == '__main__':


    print __doc__ % {'scriptName' : sys.argv[0]}
  
    import doctest
    
#    doctest.testmod(verbose=True)
    doctest.run_docstring_examples(trie_1, globals(),verbose=True)


    import os
    cmd='rm *.so *.db'
    os.system(cmd)

