
#include "trie.hpp"
#include "tools/tools-text.hpp"



/*
 * Algorithm: Edit distance using a trie-tree (Dynamic Programming)
*/




namespace trees {

	using namespace std;

	


	void trie::insert(string w) {
		string _w(w);
		trie_detail::transform(_w);
		_w = string("$") + _w;
	        int sz = _w.size();
        	trie* n = this;
	        for (int i = 0; i < sz; ++i) {
        	    if (n->next.find(_w[i]) == n->next.end()) {
                	n->next[_w[i]] = new trie();
	            }
 
        	    n = n->next[_w[i]];
	            }
 
	        n->word = w;
	        n->counter++;
	    }


	bool trie::remove(const string & w)
	{




		string _w(w);
		trie_detail::transform(_w);

		_w = string("$") + _w;

		int sz = _w.size();
		trie*  nodes[sz-1];

		trie* n = this;

		for (int i = 0; i < sz; ++i) {
			if (n->next.find(_w[i]) == n->next.end()) {
			    return false;
			}

			 if (i<sz-1) {
			 nodes[i]=n->next[_w[i]];
			}
			n = n->next[_w[i]];
		}

        n->counter--;

		if (n->counter >0) {
		    return false;
		}
		else {

		    n->word.clear();
		}



		for (int i = sz-2; i >=0; i--)
		{
			map<char, trie*>::iterator it = nodes[i]->next.find(_w[i+1]);
			if( it != nodes[i]->next.end() )
			{
			    n = it->second;
			    if ( (n->next.size()<1) && ( n->word.empty()) ) {
				 delete n;
				 nodes[i]->next.erase( it);

			    } else {
			        return true;
			    }
			} // if iterator

		} // for i from sz-2 to 0
    return true;
		} // remove





} // namespace trees::

